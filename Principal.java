 import javax.swing.JOptionPane;

 public class Principal{
   public static void main (String arg[]){        
      String modelo, placa, estado, opcion;
      int pantalla;          
      Computadora computadora = null;
      do{
 opcion = JOptionPane.showInputDialog("Men� Principal: \n"
+"\n a. Crear instancia de computadora sin valores."
+"\n b. Crear instancia de computadora con los valores necesarios para cada uno de sus atributos."
+"\n c. Modificar la placa de la computadora."
+"\n d. Modificar el modelo de la computadora."
+"\n e. Modificar el tama�o de la pantalla de la computadora."
+"\n f. Modificar el estado de la computadora."
+"\n g. Ver los datos de la computadora."
+"\n h. Salir");  

 switch (opcion){
   case "a":
     if(computadora == null){
        computadora = new Computadora();
   } else{
        JOptionPane.showMessageDialog(null, "La instancia ya fue creada");
   }    
    
 break;   
   
 case "b":
   
   modelo = JOptionPane.showInputDialog("Digite el modelo de la computadora");
   computadora.setModelo(modelo);
   placa = JOptionPane.showInputDialog("Digite la placa de la computadora");
   computadora.setPlaca(placa);
   pantalla = Integer.parseInt(JOptionPane.showInputDialog("Digite el tama�o de la pantalla"));
   computadora.setPantalla(pantalla);
   computadora = new Computadora(modelo, placa, pantalla);
  
 break;
  
 case "c":
   if(computadora != null) {
     placa = JOptionPane.showInputDialog("Nueva placa: ");
     computadora.setPlaca(placa);
  }else{
     JOptionPane.showMessageDialog(null, "La instancia ya fue creada");
  }
 break;
 case "d":
   if(computadora == null) {
     JOptionPane.showMessageDialog(null, "La instancia no ha sido creada");
   }else{
     modelo = JOptionPane.showInputDialog("Nuevo modelo: ");
     computadora.setModelo(modelo);
   }    
 break;
 case "e":  
   if(computadora == null) {
   JOptionPane.showMessageDialog(null, "La instancia no ha sido creada");
 }else{
   pantalla = Integer.parseInt(JOptionPane.showInputDialog("Nuevo tama�o de computadora: "));
   computadora.setPantalla(pantalla);
 }
 break;
     
 case "f":
    if(computadora == null) {
   JOptionPane.showMessageDialog(null, "La instancia no ha sido creada");
 }else{
   estado = JOptionPane.showInputDialog("Nuevo estado de la computadora: ");
   computadora.setEstado(estado);
 }
 break;
     
 case "g":
   if (computadora != null){
   JOptionPane.showMessageDialog(null,"Informaci�n de la computadora: \n" + computadora.toString());
 }else{
     JOptionPane.showMessageDialog(null, "No hay valores");
 }
 break;
 
 case "h":
      JOptionPane.showMessageDialog(null, "Hasta luego");
 break;
 default:
       JOptionPane.showMessageDialog(null,"Esa opci�n no se encuentra disponible. ");
  }//Fin switch
 }while(!opcion.equals("h"));//Fin while
}//Fin del main
}//Fin de la clase
